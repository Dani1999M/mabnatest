<?php

namespace App\Http\Controllers;

use App\Models\FUNC1;
//use App\Http\Requests\func1\Func1 as REQUEST;
use Illuminate\Http\Request ;
class FUNC1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('func1.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start = 1;
        for ($i = 0; $i < $request['number']; $i++) {
            echo "\t";
            for ($ws = 1; $ws <= $request['number'] - $i; $ws++) {
                echo "  ";
            }
            for ($j = 0; $j <= $i; $j++) {
                if ($j == 0 || $i == 0)
                    $start = 1;
                else
                    $start = $start * ($i - $j + 1) / $j;
                echo $start . "   ";
            }
            echo "\n";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FUNC1  $fUNC1
     * @return \Illuminate\Http\Response
     */
    public function show(FUNC1 $fUNC1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FUNC1  $fUNC1
     * @return \Illuminate\Http\Response
     */
    public function edit(FUNC1 $fUNC1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FUNC1  $fUNC1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FUNC1 $fUNC1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FUNC1  $fUNC1
     * @return \Illuminate\Http\Response
     */
    public function destroy(FUNC1 $fUNC1)
    {
        //
    }
}
