<?php

namespace App\Http\Controllers;

use App\Models\FUNC2;
use Illuminate\Http\Request ;

class FUNC2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('func2.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "%d" . $request['number'];
        $pascalArray[$request['number'] + 1][$request['number'] + 1];
        $i = null;
        $j = null;
        if (0 <= $request['number'] && $request['number'] <= 20) {
            for ($i = 0; $i < $request['number'] + 1; $i++) {
                for ($j = 0; $j <= $i; $j++) {
                    if ($j == 0 || $j == $i)
                        $pascalArray[$i][$j] = 1;
                    else
                        $pascalArray[$i][$j] = $pascalArray[$i - 1][$j - 1] + $pascalArray[$i - 1][$j];
                    if ($i == $request['number'])
                        echo "%d " . $pascalArray[$i][$j];
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\FUNC2 $fUNC2
     * @return \Illuminate\Http\Response
     */
    public
    function show(FUNC2 $fUNC2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\FUNC2 $fUNC2
     * @return \Illuminate\Http\Response
     */
    public
    function edit(FUNC2 $fUNC2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\FUNC2 $fUNC2
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, FUNC2 $fUNC2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\FUNC2 $fUNC2
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(FUNC2 $fUNC2)
    {
        //
    }
}
