<?php

namespace App\Http\Requests\func1;

use Illuminate\Foundation\Http\FormRequest;

class Func1 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|number'
        ];
    }

    public function messages(): array
    {
        return [
            'number.number' => 'لطفا عدد واردکنید'
        ];
    }
}
