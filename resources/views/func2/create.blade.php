@extends('welcome')
@section('title')
روش دوم
@endsection
@section('content')
    <div class="container">
        <form action="{{route('func2.store')}}" method="POST" >
            @csrf
            <div class="form-group">
                <input type="text" name="number" id="number" class="form-control">
            </div>
            <div class="center">
                <button type="submit" class="btn btn-success">ارسال</button>
            </div>
        </form>
    </div>
@endsection
